package it.unibo.oop.lab.exception2;

import org.junit.Test;

/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {

    /**
     * Used to test Exceptions on {@link StrictBankAccount}.
     */
    @Test
    public void testBankOperations() {
        /*
         * 1) Creare due StrictBankAccountImpl assegnati a due AccountHolder a
         * scelta, con ammontare iniziale pari a 10000 e nMaxATMTransactions=10
         * 
         * 2) Effetture un numero di operazioni a piacere per verificare che le
         * eccezioni create vengano lanciate soltanto quando opportuno, cioè in
         * presenza di un id utente errato, oppure al superamento del numero di
         * operazioni ATM gratuite.
         */
    	AccountHolder acc1 = new AccountHolder("Pippo", "Baudo", 111);
    	AccountHolder acc2 = new AccountHolder("Gianni", "Morandi", 222);
    	BankAccount bankAcc1 = new StrictBankAccount(acc1.getUserID(), 10000, 10);
    	BankAccount bankAcc2 = new StrictBankAccount(acc2.getUserID(), 10000, 10);
    	
    	for(int i = 0; i < 9; i++) {
    		bankAcc1.withdraw(111, 1);
    		bankAcc2.withdraw(222, 1);
    	}
    	System.out.println(bankAcc1.toString() + "\n------------------\n" + bankAcc2.toString());
    	bankAcc1.deposit(111, 10);
    	bankAcc2.withdrawFromATM(222, 1000000);
    	System.out.println(bankAcc1.toString() + "\n------------------\n" + bankAcc2.toString());
    }
}
