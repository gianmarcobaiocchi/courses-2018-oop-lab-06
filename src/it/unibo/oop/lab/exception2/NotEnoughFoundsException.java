package it.unibo.oop.lab.exception2;

public class NotEnoughFoundsException extends UnsupportedOperationException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final double balance;
	
	public NotEnoughFoundsException(final double balance) {
		this.balance = balance;
	}
	
	public String getMessage() {
		return "Bilancio: " + this.balance + " non abbastanza!";
	}

}
