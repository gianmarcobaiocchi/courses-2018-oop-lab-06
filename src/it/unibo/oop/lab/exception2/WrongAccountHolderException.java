package it.unibo.oop.lab.exception2;

public class WrongAccountHolderException extends SecurityException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int account;
	
	public WrongAccountHolderException(final int account) {
		super();
		this.account = account;
	}
	
	public String getMessage() {
		return "L'account: " + this.account + " non ha il permesso di eseguire questa operazione!";
	}
}
