package it.unibo.oop.lab.exception2;

public class TransactionsOverQuotaException extends UnsupportedOperationException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int nMax;
	
	public TransactionsOverQuotaException(final int nMax) {
		super();
		this.nMax = nMax;
	}
	
	public String getMessage() {
		return "Raggiunto il numero massimo di transazioni : " + this.nMax + "!";
	}
	
}
