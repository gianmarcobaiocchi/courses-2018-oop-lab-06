/**
 * 
 */
package it.unibo.oop.lab.collections2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.runners.Parameterized.UseParametersRunnerFactory;

/**
 * 
 * Instructions
 * 
 * This will be an implementation of
 * {@link it.unibo.oop.lab.collections2.SocialNetworkUser}:
 * 
 * 1) complete the definition of the methods by following the suggestions
 * included in the comments below.
 * 
 * @param <U>
 *            Specific user type
 */
public class SocialNetworkUserImpl<U extends User> extends UserImpl implements SocialNetworkUser<U> {

    /*
     * 
     * [FIELDS]
     * 
     * Define any necessary field
     * 
     * In order to save the people followed by a user organized in groups, adopt
     * a generic-type Map:
     * 
     * think of what type of keys and values would best suit the requirements
     */
	private List<String> groups = new ArrayList<>();
	private List<Map<String, U>> followers = new ArrayList<>();
	private int arrayLong;
    /*
     * [CONSTRUCTORS]
     * 
     * 1) Complete the definition of the constructor below, for building a user
     * participating in a social network, with 4 parameters, initializing:
     * 
     * - firstName - lastName - username - age and every other necessary field
     * 
     * 2) Define a further constructor where age is defaulted to -1
     */
	
	
	public SocialNetworkUserImpl(final String firstName, final String lastName, final String username, final int age){
		super(firstName, lastName, username, age);
		arrayLong = 0;
	}
	
	public SocialNetworkUserImpl(final String firstName, final String lastName, final String username) {
		super(firstName, lastName, username);
		arrayLong = 0;
	}

    /**
     * Builds a new {@link SocialNetworkUserImpl}.
     * 
     * @param name
     *            the user firstname
     * @param surname
     *            the user lastname
     * @param userAge
     *            user's age
     * @param user
     *            alias of the user, i.e. the way a user is identified on an
     *            application
     */

    /*
     * [METHODS]
     * 
     * Implements the methods below
     */
	@Override
    public boolean addFollowedUser(final String circle, final U user) {
		if(groups.contains(circle)) {
			final Map<String, U> map = followers.get(groups.indexOf(circle));
			if(map.containsValue(user)) {
				return false;
			}else {
				map.put(user.getUsername(), user);
				return true;
			}
		}else {
			groups.add(arrayLong, circle);
			final Map<String, U> newMap = new HashMap<String, U>();
			newMap.put(user.getUsername(), user);
			followers.add(arrayLong,newMap);
			arrayLong = arrayLong + 1;
			return true;
		}
    }

    @Override
    public Collection<U> getFollowedUsersInGroup(final String groupName) {
    	Collection<U> coll = new ArrayList<>();
    	if(groups.contains(groupName)) {
    		final Map<String, U> map = followers.get(groups.indexOf(groupName));
    		coll.addAll(map.values());
    		return coll;
    	}
    	return coll;
    }

    @Override
    public List<U> getFollowedUsers() {
    	List<U> list = new ArrayList<>();
    	for(Map<String, U> map : followers) {
    		list.addAll(map.values());
    	}
        return list;
    }

}
